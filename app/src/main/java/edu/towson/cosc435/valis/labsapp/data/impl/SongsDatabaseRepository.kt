package edu.towson.cosc435.valis.labsapp.data.impl

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.valis.labsapp.data.ISongsRepository
import edu.towson.cosc435.valis.labsapp.data.SongsDatabase
import edu.towson.cosc435.valis.labsapp.model.Song

class SongsDatabaseRepository(ctx: Context) : ISongsRepository {
    private val db: SongsDatabase

    init {
        db = Room.databaseBuilder(
            ctx,
            SongsDatabase::class.java,
            "songs.db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    override suspend fun getSongs(): List<Song> {
        return db.songsDao().getSongs()
    }

    override suspend fun deleteSong(idx: Int) {
//        TODO("Not yet implemented")
    }

    override suspend fun addSong(song: Song) {
        return db.songsDao().addSong(song)
    }

    override suspend fun toggleAwesome(idx: Int) {
//        TODO("Not yet implemented")
    }

}