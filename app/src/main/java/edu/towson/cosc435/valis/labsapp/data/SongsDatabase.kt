package edu.towson.cosc435.valis.labsapp.data

import androidx.room.*
import edu.towson.cosc435.valis.labsapp.model.Song

@Dao
interface SongsDao {
    @Query("select * from songs")
    fun getSongs(): List<Song>

    @Insert
    suspend fun addSong(song: Song)

    @Delete
    suspend fun deleteSong(song: Song)

    @Update
    suspend fun updateSong(song: Song)

}

@Database(entities = [Song::class], version = 1, exportSchema = false)
abstract class SongsDatabase: RoomDatabase() {
    abstract fun songsDao(): SongsDao
}