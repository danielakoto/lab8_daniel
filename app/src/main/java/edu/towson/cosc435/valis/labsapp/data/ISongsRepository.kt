package edu.towson.cosc435.valis.labsapp.data

import edu.towson.cosc435.valis.labsapp.model.Song

interface ISongsRepository {
    suspend fun getSongs(): List<Song>
    suspend fun deleteSong(idx: Int)
    suspend fun addSong(song: Song)
    suspend fun toggleAwesome(idx: Int)
}